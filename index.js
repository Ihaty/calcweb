var display = document.getElementById('display');
var clear = document.getElementById('C');
var del = document.getElementById('delete');
var seven = document.getElementById('num7');
var eight = document.getElementById('num8');
var nine = document.getElementById('num9');
var multi = document.getElementById('multiplication');
var four = document.getElementById('num4');
var five = document.getElementById('num5');
var six = document.getElementById('num6');
var div = document.getElementById('division');
var one = document.getElementById('num1');
var two = document.getElementById('num2');
var three = document.getElementById('num3');
var minus = document.getElementById('minus');
var zero = document.getElementById('num0');
var equal = document.getElementById('=');

console.log(display.value.length);

function touch() {
  if (display.value == "0") {
    display.value = "0.";
  } else if (display.value.includes(".")){
    return;
  } else {
    display.value += ".";
  }
  console.log(display.value.length);
}
function clearAll() {
  if (display.value != 0) {
    display.value = 0;
  }
}

function deleting(event) {
  if(display.value.length == 1) {
    display.value = 0;
  } else {
    display.value = display.value.slice(0, -1);
  }
}


function setDisplayValue(event) {
  if (display.value == "0") {
    display.value = '';
  }
  if (
  event.target.value === "+" ||
  event.target.value === "-" ||
  event.target.value === "*" ||
  event.target.value === "/"
) {
  if (getOperatorInput()) {
    return;
  }
}

  display.value += event.target.value;
}

function getResult(val1, val2, oper) {
  var result;
  var num1 = Number(val1);
  var num2 = Number(val2);

  switch (oper) {
    case "+": result = num1 + num2; break;
    case "-": result = num1 - num2; break;
    case "/": result = num1 / num2; break;
    case "*": result = num1 * num2; break;
  }
  return result;
}

function getReadInput() {

  var arr;
  var oper;

  if (display.value.includes("+")) {
    arr = display.value.split("+");
    oper="+";
  } else if (display.value.includes("-")) {
    arr = display.value.split("-");
    oper="-";
  } else if (display.value.includes("/")) {
    arr = display.value.split("/");
    oper="/";
  } else if (display.value.includes("*")) {
    arr = display.value.split("*");
    oper="*";
  }
  display.value = getResult(arr[0], arr[1], oper);
}

function getOperatorInput() {
  var bool;

  if (
    display.value.includes("+") ||
    display.value.includes("-") ||
    display.value.includes("*") ||
    display.value.includes("/")
  ) {
    bool = true;
  } else {
    bool = false;
  }
  return bool;
}

seven.addEventListener('click', setDisplayValue);
eight.addEventListener('click', setDisplayValue);
nine.addEventListener('click', setDisplayValue);
multi.addEventListener('click', setDisplayValue);
four.addEventListener('click', setDisplayValue);
five.addEventListener('click', setDisplayValue);
six.addEventListener('click', setDisplayValue);
div.addEventListener('click', setDisplayValue);
one.addEventListener('click', setDisplayValue);
two.addEventListener('click', setDisplayValue);
three.addEventListener('click', setDisplayValue);
minus.addEventListener('click', setDisplayValue);
zero.addEventListener('click', setDisplayValue);
plus.addEventListener('click', setDisplayValue);
equal.addEventListener('click', getReadInput);
del.addEventListener('click', deleting);
